# Prepare Ubuntu package

## Building GEval binary
You need [Haskell Stack](https://github.com/commercialhaskell/stack).
You can install Stack with your package manager or with:

    curl -sSL https://get.haskellstack.org/ | sh

Also you will need some dependencies:

    sudo apt-get install build-essential libbz2-dev libcairo2-dev pkg-config liblzma-dev libpq-dev libpcre3-dev

When you've got Haskell Stack, [build GEval binary](https://gitlab.com/filipg/geval).

## Create DEB file
Replece the file `geval/usr/bin/REPLACE_WITH_BINARY` with geval binary and change its permisions:

    chmod 755 <geval bianry> 
    sudo chown root <geval bianry> 
    sudo chgrp root <geval bianry> 

Update file `geval/DEBIAN/control` with information about version.

    Version: *.*.*.*

Leave the folder `geval` and execute command on the project directory:

    dpkg -b geval

Install package along with any dependencies it needs execute using the following command.

    sudo apt-get install ./geval.deb

To get information about the binary package execute the following command:

    sudo dpkg -I geval.deb